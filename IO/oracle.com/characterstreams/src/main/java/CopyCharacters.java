import java.io.Reader;
import java.io.Writer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyCharacters {
    public static void main(String[] args) throws IOException {

        Reader reader = null;
        Writer writer = null;

        try {
            reader = new FileReader(".\\src\\main\\resources\\xanadu.txt");
            writer = new FileWriter(".\\build\\tmp\\characteroutput.txt");

            int c;
            while ((c = reader.read()) != -1) {
                writer.write(c);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
}

// http://docs.oracle.com/javase/tutorial/essential/io/charstreams.html