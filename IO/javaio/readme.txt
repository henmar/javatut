http://techbus.safaribooksonline.com/book/programming/java/0596527500

Java I/O, 2nd Edition
By: Elliotte Rusty Harold
Publisher: O'Reilly Media, Inc.
Pub. Date: May 16, 2006
Print ISBN-13: 978-0-596-52750-1
Print ISBN-10: 0-596-52750-0
Pages in Print Edition: 728