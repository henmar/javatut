import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.Random;

/* Closing Output Streams
 * 
 * http://techbus.safaribooksonline.com/book/programming/java/0596527500
 */

public class GenerateRandomData {
  public static void main(String[] args) {
    
    OutputStream out = null;
    
    try {
      out = new FileOutputStream(".\\build\\tmp\\numbers.dat");
      for (int i = 0; i < 100; i++){
        out.write(randInt(32, 127)); // write to the stream
        out.write('\n'); // new line
      }
    }
    catch (IOException ex) {
      System.err.println(ex);
    }
    finally {
      if (out != null){
        try {
          out.close();
        } catch (IOException ex){
          System.err.println(ex); 
            
        }
      }
    }
  }
  
    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {

      // NOTE: Usually this should be a field rather than a method
      // variable so that it is not re-seeded every call.
      Random rand = new Random();
    
      // nextInt is normally exclusive of the top value,
      // so add 1 to make it inclusive
      int randomNum = rand.nextInt((max - min) + 1) + min;
    
      return randomNum;
    }
    
    //http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java
  
}