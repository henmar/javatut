

import static org.junit.Assert.assertEquals

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test

class MyGroovyIOUnitTest {

	@Test
	void testRead() {
		def unit = new MyIOUnit()

		byte[] data = "123,456,789,123,456,789".getBytes()

		InputStream input = new ByteArrayInputStream(data)

		unit.read(input)

		assert unit.tokens[0] == '123'
		assert unit.tokens[1] == '456'
		assert unit.tokens[2] == '789'
		assert unit.tokens[3] == '123'
		assert unit.tokens[4] == '456'
		assert unit.tokens[5] == '789'
		
	}
	
	@Test
	public void testWrite() throws IOException {
		def unit = new MyIOUnit()

		ByteArrayOutputStream output = new ByteArrayOutputStream()

		unit.tokens << "one"
		unit.tokens << "two"
		unit.tokens << "three"

		unit.write(output)

		def string = new String(output.toByteArray());
		assert string == "one,two,three" 
	}
	
	
}
