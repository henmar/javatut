Examples copied from/ inspired by http://tutorials.jenkov.com/java-io/arrays.html

Demonstrates: 
-construction and usage of ByteArrayInputStream, ByteArrayOutputStream, CharArrayWriter and CharArrayReader
-testing a java class with a groovy testclass
-construction and usage of ByteArrayInputStream, ByteArrayOutputStream, CharArrayWriter and CharArrayReader in groovy



reference:
http://docs.codehaus.org/display/GROOVY/JN2025-Streams