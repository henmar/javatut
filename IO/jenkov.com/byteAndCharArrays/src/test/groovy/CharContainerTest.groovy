
import static org.junit.Assert.assertEquals

import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test

class CharContainerTest {

	@Test
	void testStoreAndRetrieve1() {
		def container = new CharContainer()
		container.store(new CharArrayReader(['H','e','l','l','o',' ','w','o','r','l','d'] as char[]))
		
		def result = new CharArrayWriter()
		container.retrieve(result)
		def resultString = new String(result.toCharArray())
		
		assert resultString == 'Hello world'
	}
	
	@Test
	public void testStoreAndRetrieve2() throws IOException {
		def container = new CharContainer()
		container.store(new CharArrayReader(['I','/','O'] as char[]))
		
		CharArrayWriter result = new CharArrayWriter()
		container.retrieve(result)
		def resultString = new String(result.toCharArray());
		
		assert resultString == "I/O" 
	}
	
	@Test
	void testStoreAndRetrieveString() {
		def container = new CharContainer()
		container.store(new CharArrayReader('Lorem ipsum dolor sit amet.'.getChars()))
		
		def result = new CharArrayWriter()
		container.retrieve(result)
		def resultString = new String(result.toCharArray())
		
		assert resultString == 'Lorem ipsum dolor sit amet.'
	}
	
	
}
//http://docs.codehaus.org/display/GROOVY/JN2025-Streams