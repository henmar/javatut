import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ByteContainer {

	private byte[] bytes = null; 

    public void store(InputStream input) throws IOException {
    	ByteArrayOutputStream output = new ByteArrayOutputStream(); //The byteArrayOutputStream is used as an array builder
    	
    	// read all bytes from inputStream to ByteArrayOutputStream
        int data = input.read();
        while(data != -1){
        	output.write(data);
        	//read next byte
            data = input.read();
        }
        
        // let ByteArrayOutputStream build an array from the bytes sent
        bytes = output.toByteArray();
    }
    
    public void retrieve(OutputStream output) throws IOException {
    	//construct a ByteArrayInputStream from the byteArray instance variable - of course slightly contrived, but for the sake of the example...
    	InputStream input = new ByteArrayInputStream(bytes);
    	
    	//	each byte read from the ByteArrayInputStrem is written to the OutputStream
    	int data = input.read();
    	while(data != -1) {
    	    output.write(data);
    	    //read next byte
    	    data = input.read();
    	}
    }
    
}

//http://tutorials.jenkov.com/java-io/arrays.html
//http://tutorials.jenkov.com/java-unit-testing/io-testing.html