import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;


public class CharContainer {

	private char[] chars = null; 

    public void store(CharArrayReader input) throws IOException {
    	CharArrayWriter output = new CharArrayWriter(); //The charArrayWriter is used as an array builder
    	
    	// read all chars from reader, write them to the writer
        int c = input.read();
        while(c != -1){
        	output.write(c);
        	//read next byte
            c = input.read();
        }
        
        // let CharArrayWriter build an array from the chars
        chars = output.toCharArray();
    }
    
    public void retrieve(CharArrayWriter output) throws IOException {
    	//construct a CharArrayReader from the charArray instance variable - of course slightly contrived, but for the sake of the example...
    	CharArrayReader input = new CharArrayReader(chars);
    	
    	//	each char read from the CharArrayReader is written to the CharArrayWriter
    	int c = input.read();
    	while(c != -1) {
    	    output.write(c);
    	    //read next char
    	    c = input.read();
    	}
    }
    
}

//http://tutorials.jenkov.com/java-io/arrays.html
//http://tutorials.jenkov.com/java-unit-testing/io-testing.html