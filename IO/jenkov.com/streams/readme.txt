Examples copied from/inspired by http://tutorials.jenkov.com/java-io/streams.html

Demonstrates: 
-construction and usage of FileInputStream and FileOutputStream
-testing a java class with a groovy testclass



reference:
http://docs.codehaus.org/display/GROOVY/JN2025-Streams
http://www.mkyong.com/java/how-to-write-to-file-in-java-fileoutputstream-example/