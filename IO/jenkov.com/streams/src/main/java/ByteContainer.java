import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ByteContainer {

	private static final String FILE_NAME = ".\\build\\tmp\\bytes.txt";
	
	public ByteContainer(){
		File file = new File(FILE_NAME);
		
		if (file.exists()){
			boolean success = file.delete();
			if (!success) {
				throw new IllegalStateException();
			}
		}
	}

	public void store(InputStream input) {
		File file = new File(FILE_NAME);
		OutputStream output = null;
		try {
			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			output = new FileOutputStream(file);

			// read all bytes from inputStream one by one and write them to FileOutputStream
			int data = input.read();
			while(data != -1){
				output.write(data);
				//read next byte
				data = input.read();
			}
			output.flush();
			output.close();
		} catch (FileNotFoundException fnfe){
			fnfe.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		} finally {
			try { if (output != null){ output.close(); }} catch (IOException ioe){}
		}
	}

	public void retrieve(OutputStream output){
		File file = new File(FILE_NAME);
		InputStream input = null;
		try {
			// if file doesn't exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			input = new FileInputStream(file);

			//	each byte read from the FileInputStrem is written to the OutputStream
			int data = input.read();
			while(data != -1) {
				output.write(data);
				//read next byte
				data = input.read();
			}
			input.close(); //FileInputStream must be closed, or it will not be possible to delete the file.
			output.flush();
			output.close();
		} catch (IOException ioe){
			ioe.printStackTrace();
		} finally {
			try { 
				if (input != null){ input.close(); }
				if (output != null){ output.close(); }
				} catch (IOException ioe){}
		}
	}

}

//http://tutorials.jenkov.com/java-io/streams.html
//http://tutorials.jenkov.com/java-unit-testing/io-testing.html
//http://www.mkyong.com/java/how-to-write-to-file-in-java-fileoutputstream-example/