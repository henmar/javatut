
import static org.junit.Assert.assertEquals

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test

class ByteContainerTest {

	@Test
	void testStoreAndRetrieve1() {
		def container = new ByteContainer()
		container.store(new ByteArrayInputStream([72,101,108,108,111,32,119,111,114,108,100] as byte[]))
		
		def result = new ByteArrayOutputStream()
		container.retrieve(result)
		def resultString = new String(result.toByteArray())
		
		assert resultString == 'Hello world'
	}
	
	@Test
	public void testStoreAndRetrieve2() throws IOException {
		def container = new ByteContainer()
		container.store(new ByteArrayInputStream([73,47,79] as byte[]))
		
		ByteArrayOutputStream result = new ByteArrayOutputStream()
		container.retrieve(result)
		def resultString = new String(result.toByteArray());
		
		assert resultString == "I/O" 
	}
	
	@Test
	public void testRetrieveEmptyByteArrayFromEmptyByteContainer() throws IOException {
		def container = new ByteContainer()
		
		ByteArrayOutputStream result = new ByteArrayOutputStream()
		container.retrieve(result)
		def resultString = new String(result.toByteArray());
		
		assert resultString == ""
	}
	
	
}
//http://docs.codehaus.org/display/GROOVY/JN2025-Streams